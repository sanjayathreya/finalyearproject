%clear all
function corrcoff(refe,imga)

a=refe;
ma=mean(a);
mx=mean(ma);
%ouc=im2double(ouc);
%ouc=real(ouc);
b=imga;
mb=mean(b);
my=mean(mb);


[x,y]=size(a);
x_co(x,y)=0;
y_co(x,y)=0;

for i =1:x
    for j=1:y
       x_co(i,j)=a(i,j)-mx;
       y_co(i,j)=b(i,j)-my;
    end
end

t1=x_co*y_co;
t2=sum(sum(t1));
p1=x_co^2;
p2=sum(sum(p1));
q1=y_co^2;
q2=sum(sum(q1));
d=p2*q2;
d1=sqrt(abs(d));
coeff=abs(p2/d1);
