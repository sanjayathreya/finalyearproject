function [ psnr ] = psnr_dee( rmse)
psnr = 20.*log10(255/rmse);

end
