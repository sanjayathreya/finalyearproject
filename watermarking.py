%-----------------------------------------------------------------------
% CODE FOR DIGITAL WATERMARKING
%-------------------------------------------------------------------

close all                    %closes all existing files 
clear all                    %clears data from workspace
clc                          %clears data from command window

res(1000)=0;                 %extrapulates data onto 
res1(1000)=0;                %excel sheet

%vary alp from 300-500 to get the common PSNR value
for alp = 1    
    wav = 'sym4'
    %scale factor of SVD, can be any value > 1.
    
 % The Host image
 %---------------------------------------------------------
 
 
a=imread('peppers.bmp');       
% a = imresize(x,[256 256]);   %if images arent of the same size then resize

a=im2double(a);               %convert to double

%split the image into R, G and B channels
r=a(:,:,1);
g=a(:,:,2);
b=a(:,:,3);


% subplot(221),imshow(a);
% title('image');
% subplot(222),imshow(r);
% title('R');
% subplot(223),imshow(g);
% title('G');
% subplot(224),imshow(b);
% title('B');


%Discrete Wavelet Transform on LL, LH, HL, HH
% r, g, b represent the channels, uncomment the necessory channel used
[ll lh hl hh]=dwt2(r,wav);
% [ll lh hl hh]=dwt2(g,'sym3');
% [ll lh hl hh]=dwt2(b,'sym3');


% figure
% subplot(221)
% imshow(ll)
% title('LL');
% subplot(222)
% imshow(lh)               % plots of LL, LH, HL, HH
% title('LH');
% subplot(223)
% imshow(hl)
% title('HL');
% subplot(224)
% imshow(hh)
% title('HH');
% 
% %Discrete Fourier Transform on the selected sub-band
 hldft=fft2(lh);
% 
%SVD is done on the output of the fourier transform
[u s v]=svd(hldft);
%  imshow( hldft)
%  
 
 
 %---------------------------------------------------------------
%The Watermark Image
%-------------------------------------------------------------
im2=imread('boat.tiff');  %read watermark image
%  im2=rgb2gray(im2);          %if image is a color image
im2=im2double(im2);
%im3=cos(im2)

%Discrete WAvelet Transform on the image to get the sub-bands
[ll1,lh1,hl1,hh1]=dwt2(im2,wav);

%Discrete Cosine Transform is applied on the desired sub band
hl1dft=dct2(lh1);
% imshow(hl1dft)

%SVD is then used on the output of the dct
[u1,s1,v1]=svd(hl1dft);
% figure 
% imshow(hl1dft)

%combining entities of host and watermark images
%--------------------------------------------------------
s2=s+alp*s1;            
c=u*s2*v';

cift=ifft2(c);
% figure
% imshow(cift);
out=idwt2(ll,cift,hl,hh,wav);
% figure
% imshow(ll)

%Uncomment the respective sets for the respective channels
%----------------------------------------------------------
ou(:,:,1)=out(:,:);
ou(:,:,2)=g;
ou(:,:,3)=b;

% ou(:,:,1)=r;
% ou(:,:,2)=out(:,:);
% ou(:,:,3)=b;
% 
% ou(:,:,1)=r;
% ou(:,:,2)=g;
% ou(:,:,3)=out(:,:);

% imshow(ou);
imwrite(ou,'ou.jpg','jpg')


%---------------------------------------------------------------
%Watermark Extraction
%-------------------------------------------------------------------
ab=imread('ou.jpg');
ra=ab(:,:,1);     %red channel
ga=ab(:,:,2);     %green channel
ba=ab(:,:,3);     %blue channel

%Sub-band selection, uncomment the necessory channel required
%---------------------------------------------------------
[ll2,lh2,hl2,hh2]=dwt2(ra,wav);
% [ll2,lh2,hl2,hh2]=dwt2(ga,'sym3');
% [ll2,lh2,hl2,hh2]=dwt2(ba,'sym3');


hl2ft=fft2(lh2);           %Fourier transform to the output of DWT
[u3,s3,v3]=svd(hl2ft);     %svd of the watermarked image

%Seperating the host and watermark from the watermarked image
%----------------------------------------------------------
s4=(s3-s)/alp;             
ca=u1*s4*v1';
caift=ifft2(ca);
% figure
%  imshow(caift)
ouc=idwt2(ll1,caift,hl1,hh1,wav);
%ouc=acos(ouc1)
% figure
% imshow(ouc);
rmse=RMSE(a,ou);
p=2*real(psnr_dee(rmse))    %PSNR of host image
%corrcoff(im2,ouc);
res(alp)=p;
rmse=RMSE(im2,ouc);
p1=2*real(psnr_dee(rmse))   %PSNR of watermark image
res1(alp)=p1;
%comet(res)
end
% plot(res,alp)
%  figure
%  plot(res)               % the point of contact of the two
%  hold on                 % graphs gives the best scale factor value
%  plot(res1)
