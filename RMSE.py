function [ rmse ] = RMSE( Original,Host )
[m,n,l] = size(Original);
mse = 0;
for i = 1:m
    for j = 1:n
        for k = 1:l
        mse = mse + (Original(i,j,k) - Host(i,j,k)).*(Original(i,j,k) - Host(i,j,k));
    end
end
rmse = sqrt(mse./(m*n*l));


end
